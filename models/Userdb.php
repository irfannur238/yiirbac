<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $iduser
 * @property string $username
 * @property string $password
 * @property string $authkey
 * @property string $email
 * @property string|null $authkey
 * @property string|null $name
 * @property string|null $email
 * @property string|null $privilege
 */
class Userdb extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['username', 'password', 'privilege'], 'required'],
            [['username', 'password', 'name', 'email'], 'string', 'max' => 50],
            [['authkey'], 'string', 'max' => 100],
            [['privilege'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'iduser' => 'Iduser',
            'username' => 'Username',
            'password' => 'Password',
            'authkey' => 'Authkey',
            'name' => 'Name',
            'email' => 'Email',
            'privilege' => 'Privilege',
        ];
    }

}
