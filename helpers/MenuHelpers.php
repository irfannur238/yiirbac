<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\helpers;

use app\models\User;
use app\modules\auth\models\AuthItem;
use app\modules\auth\models\AuthItemChild;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class MenuHelpers {

    public static function getMenus() {
        $dataRoute = self::myRoutesDet();
        $allowRoutes = $dataRoute['routes'];
        $labels = self::defaultMenus();

        $items = [];
        foreach ($labels as $label) {
            $labelUrl = $label['url'];
            if (isset($label['child'])) {
                $child = [];
                foreach ($label['child'] as $c) {
                    if (in_array($c['url'], $allowRoutes)) {
                        $child[] = ['label' => $c['label'], 'url' => ['/' . $c['url']]];
                    }
                }
                if ($child) {
                    $items[] = ['label' => $label['label'], 'url' => ['/' . $labelUrl],
                        'items' => $child,
                    ];
                }
            } else {
                if (in_array($labelUrl, $allowRoutes)) {
                    $items[] = ['label' => $label['label'], 'url' => ['/' . $labelUrl]];
                }
            }
        }

        return $items;
    }

    public static function defaultMenus() {
        $menus = [
            ['label' => 'posts', 'url' => '',
                'child' => [
                    ['label' => 'Post', 'url' => 'post/post/index'],
                    ['label' => 'Approval', 'url' => 'approval/approval/index'],
                    ['label' => 'Content', 'url' => 'post/content/index'],
                ]
            ],
            ['label' => 'Routes Permission', 'url' => 'auth/rbacroutepermission/index'],
            ['label' => 'Route Assignment', 'url' => 'auth/rbacrouteassignment/index'],
            ['label' => 'User Assignment', 'url' => 'auth/userassignment/index']
        ];
        return $menus;
    }

    public static function getMyAllowedRoutes($rolename) {
        $routeAllowed = (new Query)
                ->select(['*'])
                ->from(['AI' => AuthItem::tableName()])
                ->innerJoin(['AC' => AuthItemChild::tableName()], 'AI.name = AC.child')
                ->where(['AC.parent' => $rolename])
                ->andWhere(['AI.type' => 2])
                ->andWhere(['is', 'AI.rule_name', new Expression('null')])
                ->all();
        return $routeAllowed;
    }

    public static function myRoutesDet() {
        $myRoute = self::getMyAllowedRoutes(User::Myprivilege());
        $modules = [];
        foreach ($myRoute as $perRoute) {
            $getModule = self::findByRoute($perRoute['name'])['module'];
            if ($getModule) {
                $modules[] = $getModule;
            }
        }
        return [
            'modules' => $modules,
            'routes' => ArrayHelper::getColumn($myRoute, 'name'),
        ];
    }

    public static function findByRoute($route) {
        $route = explode('/', $route);
        $module = isset($route[0]) ? $route[0] : false;
        $controller = isset($route[1]) ? $route[1] : false;
        $action = isset($route[2]) ? $route[2] : false;

        return [
            'module' => $module,
            'controller' => $controller,
            'action' => $action,
        ];
    }

    public static function getMenus_deprecated() {
        $dataRoute = self::myRoutesDet();
        $modules = array_unique($dataRoute['modules']);
        $routes = $dataRoute['routes'];

        $items = [];
        foreach ($modules as $m) {
            $rt = [];
            foreach ($routes as $k => $r) {
                if (self::findByRoute($r)['module'] == $m) {
                    $rt[] = ['label' => self::findByRoute($r)['action'], 'url' => ['/' . $r]];
                }
            }
            if (!empty($rt)) {
                $items[] = ['label' => $m, 'url' => ['/' . $m],
                    'items' => $rt,
                ];
            }
        }
        $items[] = Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/auth/site/login']]) : (
                '<li>' . Html::beginForm(['/auth/site/logout'], 'post') . Html::submitButton(
                        'Logout ' . Yii::$app->user->identity->username . ' (' . User::Myprivilege() . ')', ['class' => 'btn btn-link logout']) . Html::endForm()
                . '</li>');

        return $items;
    }

}
