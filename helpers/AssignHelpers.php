<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\helpers;

class AssignHelpers {

    public static function arrayRouteAssign($data) {
        $result = [];
        foreach ($data as $k => $val) {
            $route = $val['name'];
            $route = explode('/', $route);
            $module = isset($route[0]) ? $route[0] : '-';
            $controller = isset($route[1]) ? $route[1] : '-';
            if ($k == 0) {
                $result[] = [
                    'name' => null,
                    'type' => 10,
                    'module' => $module,
                    'controller' => $controller
                ];
                $result[] = [
                    'name' => null,
                    'type' => 11,
                    'module' => $module,
                    'controller' => $controller
                ];
            }
            $result[] = $val;
            if (isset($data[$k + 1])) {
                $routeNext = $data[$k + 1]['name'];
                $routeNext = explode('/', $routeNext);
                $moduleNext = isset($routeNext[0]) ? $routeNext[0] : '-';
                $controllerNext = isset($routeNext[1]) ? $routeNext[1] : '-';
                if ($module != $moduleNext) {
                    $result[] = [
                        'name' => null,
                        'type' => 10,
                        'module' => $moduleNext,
                        'controller' => $controllerNext
                    ];
                    if ($controller != $controllerNext) {
                        $result[] = [
                            'name' => null,
                            'type' => 11,
                            'module' => $moduleNext,
                            'controller' => $controllerNext
                        ];
                    }
                } else {
                    if ($controller != $controllerNext) {
                        $result[] = [
                            'name' => null,
                            'type' => 11,
                            'module' => $moduleNext,
                            'controller' => $controllerNext
                        ];
                    }
                }
            }
        }
        return $result;
    }

}
