<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\helpers;

use Yii;

class Utils {

    public static function flash($type, $message) {
       return Yii::$app->session->setFlash($type, $message);
    }

}
