<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\helpers;

use Yii;
use yii\filters\AccessControl;

class AuthHelpers {

    public static function behaviors() {
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => TRUE,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        $module = self::getModule();
                        $controller = self::getController();
                        $action = self::getAction();
                        $route = "$module/$controller/$action";
                        //$post = Yii::$app->request->post();
                        if (Yii::$app->user->can($route)) {
                            return TRUE;
                        }
                    }
                ]
            ]
        ];
        return $behaviors;
    }

    public static function getModule() {
        return Yii::$app->controller->module->id;
    }

    public static function getController() {
        return Yii::$app->controller->id;
    }

    public static function getAction() {
        return Yii::$app->controller->action->id;
    }

    public static function getRoute() {
        return Yii::$app->requestedRoute;
    }

    public static function getRole() {
        //urutan array setelah setingan default role pada config/web bila ada
        $role = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());
        $role = isset(array_keys($role)[1]) ? array_keys($role)[1] : array_keys($role)[0];
        return $role;
    }

}
