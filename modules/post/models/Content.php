<?php

namespace app\modules\post\models;

use Yii;

/**
 * This is the model class for table "content".
 *
 * @property int $idcontent
 * @property string $name
 * @property int|null $type
 * @property string|null $description
 */
class Content extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['type'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcontent' => 'Idcontent',
            'name' => 'Name',
            'type' => 'Type',
            'description' => 'Description',
        ];
    }
}
