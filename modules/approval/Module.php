<?php

namespace app\modules\approval;

/**
 * approval module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\approval\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->layout = '@app/modules/auth/views/layouts/main';
        // custom initialization code goes here
    }
}
