<?php

namespace app\modules\auth\controllers;

use app\helpers\AssignHelpers;
use app\helpers\AuthHelpers;
use app\helpers\Utils;
use app\modules\auth\models\AuthItem;
use app\modules\auth\rules\AuthorRule;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * RbacController implements the CRUD actions for AuthItem model.
 */
class RbacroutepermissionController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return AuthHelpers::behaviors();
    }

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionIndex() {
        $data = AuthItem::find()
                ->where(['type' => 2])
                ->andWhere(['is', 'rule_name', new Expression('null')])
                ->orderBy(['name' => SORT_ASC])
                ->asArray()
                ->all();
        $result = AssignHelpers::arrayRouteAssign($data);
        $dataProvider = new ArrayDataProvider([
            'allModels' => $result,
            'key' => 'name',
            'pagination' => ['pageSize' => 100],
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AuthItem model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    private function checkRoute($route) {
        $check = AuthItem::find()
                ->where(['name' => $route])
                ->andWhere(['type' => 2])
                ->count();
        if ($check > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new AuthItem();
        $transaction = \Yii::$app->db->beginTransaction();
        $req = Yii::$app->request;

        if ($model->load(Yii::$app->request->post())) {
            $moduleCon = explode('/', $model->name);
            if (isset($moduleCon[0]) && isset($moduleCon[1])) {
                try {
                    $auth = Yii::$app->authManager;
                    $moduleController = str_replace("//", "/", "$moduleCon[0]/$moduleCon[1]/");
                    $actions = explode(';', $model->actions);
                    foreach ($actions as $value) {
                        $name = $moduleController . $value;
                        if ($this->checkRoute($name) == false) {
                            continue;
                        }
                        $route = $auth->createPermission($name);
                        $route->description = $model->description;
                        $auth->add($route);
                    }
                    Utils::flash('success', 'Route Berhasil dibuat');
                    $transaction->commit();
                } catch (Exception $e) {
                    Yii::$app->session->setFlash("danger", "Gagal Dibuat");
                    $transaction->rollBack();
                }
                return $this->redirect(['index']);
            } else {
                Utils::flash('danger', 'Silahkan Tulis Format module controller Sesuai contoh <code>module/controller</code>');
                $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $auth = Yii::$app->authManager;
        $route = $auth->createPermission($model->name);
        if ($auth->remove($route)) {
            Utils::flash('success', 'Success remove route');
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionCreate_permission() {
        $auth = Yii::$app->authManager;

        $index = $auth->createPermission('auth/post/index');
        $index->description = 'index a post';
        $auth->add($index);

        $create = $auth->createPermission('auth/post/create');
        $create->description = 'Create a post';
        $auth->add($create);

        $update = $auth->createPermission('auth/post/update');
        $update->description = 'update a post';
        $auth->add($update);

        $view = $auth->createPermission('auth/post/view');
        $view->description = 'view a post';
        $auth->add($view);

        $delete = $auth->createPermission('auth/post/delete');
        $delete->description = 'delete a post';
        $auth->add($delete);
    }

    public function actionCreate_role() {
        $auth = Yii::$app->authManager;

        $index = $auth->createPermission('auth/post/index');
        $create = $auth->createPermission('auth/post/create');
        $view = $auth->createPermission('auth/post/view');

        $update = $auth->createPermission('auth/post/update');
        $delete = $auth->createPermission('auth/post/delete');

        $author = $auth->createRole('author');
        //$auth->add($author); //for create role run this
        $auth->addChild($author, $index);
        $auth->addChild($author, $create);
        $auth->addChild($author, $view);

        $admin = $auth->createRole('admin');
        //$auth->add($admin); //for create role run this
        $auth->addChild($admin, $index);
        $auth->addChild($admin, $create);
        $auth->addChild($admin, $view);
        $auth->addChild($admin, $update);
        $auth->addChild($admin, $delete);
    }

    public function actionAssignment() {
        $auth = Yii::$app->authManager;

        $author = $auth->createRole('author');
        $admin = $auth->createRole('admin');
        $auth->assign($author, 2);
        $auth->assign($admin, 1);
    }

    public function actionCreate_rule() {
        $auth = Yii::$app->authManager;
        // add the rule
        $rule = new AuthorRule;
        $auth->add($rule);

        // add the "updateOwnPost" permission and associate the rule with it.
        $updateOwnPost = $auth->createPermission('updateOwnPost');
        $updateOwnPost->description = 'Update own post';
        $updateOwnPost->ruleName = $rule->name;
        $auth->add($updateOwnPost);

        // "updateOwnPost" will be used from "updatePost"
        $updatePost = $auth->createPermission('post/post/update');
        $auth->addChild($updateOwnPost, $updatePost);

        // allow "author" to update their own posts
        $author = $auth->createPermission('author');
        $auth->addChild($author, $updateOwnPost);
    }

}
