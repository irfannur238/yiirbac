<?php

namespace app\modules\auth\controllers;

use app\helpers\AuthHelpers;
use app\models\userdb;
use app\modules\auth\models\AuthAssignment;
use app\modules\auth\models\AuthItem;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * UserassignmentController implements the CRUD actions for userdb model.
 */
class UserassignmentController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return AuthHelpers::behaviors();
    }

    /**
     * Lists all userdb models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => userdb::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single userdb model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    private function getPrivilege() {
        $model = AuthItem::find()
                ->where(['type' => 1])
                ->all();
        return ArrayHelper::map($model, 'name', 'name');
    }

    /**
     * Creates a new userdb model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new userdb();

        if ($model->load(Yii::$app->request->post())) {
            $model->password = md5($model->password);
            if ($model->save()) {
                $auth = Yii::$app->authManager;
                $privilege = $auth->createRole($model->privilege);
                $auth->assign($privilege, $model->iduser);
                return $this->redirect(['view', 'id' => $model->iduser]);
            }
        }

        return $this->render('create', [
                    'model' => $model,
                    'privilege' => $this->getPrivilege(),
        ]);
    }

    /**
     * Updates an existing userdb model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $tmpPrivilege = $model->privilege;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->oldAttributes['password'] != $model->password) {
                $model->password = md5($model->password);
            }
            if ($model->save()) {
                if ($tmpPrivilege != $model->privilege) {
                    $auth = Yii::$app->authManager;
                    /*
                      $check = AuthAssignment::find()
                      ->where(['item_name' => $tmpPrivilege, 'user_id' => $model->iduser])
                      ->count();
                      if ($check > 0) {
                      $auth->revoke($tmpPrivilege, $model->iduser);
                      }
                     * 
                     */
                    $check2 = AuthAssignment::findOne(['user_id' => $model->iduser]);
                    if ($check2) {
                        $check2->delete();
                    }
                    $privilege = $auth->createRole($model->privilege);
                    $auth->assign($privilege, $model->iduser);
                }
                return $this->redirect(['view', 'id' => $model->iduser]);
            }
        }

        return $this->render('update', [
                    'model' => $model,
                    'privilege' => $this->getPrivilege(),
        ]);
    }

    /**
     * Deletes an existing userdb model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the userdb model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return userdb the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = userdb::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
