<?php

namespace app\modules\auth\controllers;

use app\helpers\AssignHelpers;
use app\helpers\AuthHelpers;
use app\modules\auth\models\AuthItem;
use app\modules\auth\models\AuthItemChild;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * RbacrouteassignmentController implements the CRUD actions for AuthItemChild model.
 */
class RbacrouteassignmentController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return AuthHelpers::behaviors();
    }

    /**
     * Lists all AuthItemChild models.
     * @return mixed
     */
    public function actionIndex() {
        $query = AuthItem::find()
                ->where(['type' => 1])
                ->orderBy(['name' => SORT_ASC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        $model = AuthItem::findOne($id);
        $getDataAssignment = $this->getDataAssignment($id);
        $routeAllowed = $getDataAssignment['routeAllowed'];
        $routeNotAllowed = $getDataAssignment['routeNotAllowed'];
        $resultNotallowed = AssignHelpers::arrayRouteAssign($routeNotAllowed);
        $resultAllowed = AssignHelpers::arrayRouteAssign($routeAllowed);

        $allowedDataProvider = new ArrayDataProvider([
            'allModels' => $resultAllowed,
            'key' => 'name',
            'pagination' => ['pageSize' => 50],
        ]);

        $notallowedDataProvider = new ArrayDataProvider([
            'allModels' => $resultNotallowed,
            'key' => 'name',
            'pagination' => ['pageSize' => 50],
        ]);
        return $this->render('view', [
                    'model' => $model,
                    'allowedDataProvider' => $allowedDataProvider,
                    'notallowedDataProvider' => $notallowedDataProvider,
        ]);
    }

    public function actionAssignment($privilegename, $action) {
        $transaction = \Yii::$app->db->beginTransaction();
        $req = Yii::$app->request;
        $msg = $action == 'add' ? 'Allowed' : 'Disalowed';
        if ($req->isPost) {
            try {
                $id = $_POST['data'];
                $datas = explode("-", $id);
                if ($datas) {
                    $auth = Yii::$app->authManager;
                    foreach ($datas as $perId) {
                        $route = $auth->createPermission($perId);
                        $privilege = $auth->createRole($privilegename);
                        $action == 'add' ? $auth->addChild($privilege, $route) : $auth->removeChild($privilege, $route);
                    }
                }
                Yii::$app->session->setFlash("success", "Route $msg");
                $transaction->commit();
            } catch (Exception $e) {
                Yii::$app->session->setFlash("danger", "Gagal $msg");
                $transaction->rollBack();
            }
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        } else {
            throw new NotFoundHttpException('Illegal Access.');
        }
    }

    private function getDataAssignment($id) {
        $routeAllowed = (new Query)
                ->select(['*'])
                ->from(['AI' => AuthItem::tableName()])
                ->innerJoin(['AC' => AuthItemChild::tableName()], 'AI.name = AC.child')
                ->where(['AC.parent' => $id])
                ->andWhere(['is', 'AI.rule_name', new Expression('null')])
                ->all();

        $allowedData = ArrayHelper::getColumn($routeAllowed, 'child');
        $allowed = [];
        foreach ($allowedData as $r) {
            $allowed[] = "'" . $r . "'";
        }
        $allowed = implode(', ', $allowed);
        $filter = $allowed ? "AND name NOT IN($allowed)" : null;
        $sql = "SELECT * FROM " . AuthItem::tableName() . " 
                WHERE  type = 2  AND rule_name IS NULL $filter";
        $routeNotAllowed = Yii::$app->db->createCommand($sql)->queryAll();

        return [
            'routeAllowed' => $routeAllowed,
            'routeNotAllowed' => $routeNotAllowed,
        ];
    }

    /**
     * Displays a single AuthItemChild model.
     * @param string $parent
     * @param string $child
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     *
      public function actionView($parent, $child)
      {
      return $this->render('view', [
      'model' => $this->findModel($parent, $child),
      ]);
      }
     * 
     */

    /**
     * Creates a new AuthItemChild model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new AuthItem();

        if ($model->load(Yii::$app->request->post())) {
            $auth = Yii::$app->authManager;
            $privilege = $auth->createRole($model->name);
            $privilege->description = $model->description;
            if ($auth->add($privilege)) {
                return $this->redirect(['index']);
            }
        }

        return $this->renderAjax('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing AuthItemChild model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $parent
     * @param string $child
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->renderAjax('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AuthItemChild model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $parent
     * @param string $child
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $auth = Yii::$app->authManager;
        $privilege = $auth->createRole($model->name);
        $privilege->description = $model->description;
        if ($auth->remove($privilege)) {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the AuthItemChild model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $parent
     * @param string $child
     * @return AuthItemChild the loaded model
     * @throws NotFoundHttpException if the model cannot be found

      protected function findModel($parent, $child) {
      if (($model = AuthItemChild::findOne(['parent' => $parent, 'child' => $child])) !== null) {
      return $model;
      }

      throw new NotFoundHttpException('The requested page does not exist.');
      }
     * 
     */
    protected function findModel($id) {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
