<?php

use app\modules\auth\models\AuthItemChild;
use richardfan\widget\JSRegister;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\web\YiiAsset;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this View */
/* @var $model AuthItemChild */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Route Assignment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
$columns = [
    [
        'class' => 'yii\grid\CheckboxColumn',
        'checkboxOptions' => function ($model, $key, $index, $column) {
            if ($model['type'] == 2) {
                return ['value' => $key];
            }
            return ['style' => ['display' => 'none'], 'disabled' => true];
        },
        'contentOptions' => function ($row) {
            return $row["type"] == 2 ? [] : ($row["type"] == 10 ? ['style' => 'background-color:#59b2ff;color:#fff;'] : []);
        }
    ],
    [
        'label' => 'Route',
        'format' => 'html',
        'value' => function($row) {
            return $row["type"] == 2 ? $row['name'] : ($row["type"] == 10 ? $row['module'] . ' <code>Module</code>' : $row['controller'] . ' <code>Controller</code>');
        },
        'contentOptions' => function ($row) {
            return $row["type"] == 2 ? [] : ($row["type"] == 10 ? ['style' => 'background-color:#59b2ff;color:#fff;'] : ['style' => 'background-color:#868282;color:#fff;']);
        }
    ],
];
?>
<div class="auth-item-child-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'description',
        ],
    ])
    ?>

    <div class="assignment">
        <div class="row">
            <div class="col-md-6">
                <h3>Route Allowed</h3>
                <?php
                echo GridView::widget([
                    'id' => 'allowed-routes',
                    'dataProvider' => $allowedDataProvider,
                    'columns' => $columns
                ]);
                ?>
                <?php
                $style = '#frm-disallowed{padding: 0px;margin: 0px;float: right;}';
                $this->registerCss($style);
                $form = ActiveForm::begin([
                            'id' => 'frm-disallowed',
                            'action' => "assignment?privilegename=$model->name&action=remove",
                            'method' => 'POST',
                ]);
                echo Html::hiddenInput('data', null, ['id' => 'form-disallow']);
                echo ' ' . Html::submitButton('<i class="fa fa-check"></i> Disallow', [
                    'class' => 'btn btn-danger btn-disallow',
                    'id' => 'btn-disallow',
                    'disabled' => true,
                    'data-confirm' => Yii::t('yii', 'Anda Yakin Untuk Disallow Route terpilih ?')
                ]);
                ActiveForm::end();
                ?>
            </div>
            <div class="col-md-6">
                <h3>Route Not Allowed</h3>
                <?php
                echo GridView::widget([
                    'id' => 'disallowed-routes',
                    'dataProvider' => $notallowedDataProvider,
                    'columns' => $columns,
                ]);
                ?>
                <?php
                $style = '#frm-allowed{padding: 0px;margin: 0px;float: right;}';
                $this->registerCss($style);
                $form = ActiveForm::begin([
                            'id' => 'frm-allowed',
                            'action' => "assignment?privilegename=$model->name&action=add",
                            'method' => 'POST',
                ]);
                echo Html::hiddenInput('data', null, ['id' => 'form-allow']);
                echo ' ' . Html::submitButton('<i class="fa fa-check"></i> Allow', [
                    'class' => 'btn btn-success btn-approve',
                    'id' => 'btn-allow',
                    'disabled' => true,
                    'data-confirm' => Yii::t('yii', 'Anda Yakin Untuk Allow Route terpilih ?')
                ]);
                ActiveForm::end();
                ?>
            </div>
        </div>
    </div>
</div>

<?php JSRegister::begin(); ?>
<script>
    $(document).ready(function (e) {
        var btn_allow_sel = document.getElementById('btn-allow');
        var btn_disallow_sel = document.getElementById('btn-disallow');
        btn_allow_sel.disabled = false;
        btn_disallow_sel.disabled = false;

        $('#btn-allow').on('click', function (e) {
            var form_input = document.getElementById('form-allow');
            var data = getDataAllow();
            if (data.length == 0) {
                return false;
            }
            form_input.value = data.join('-');
        });

        $('#btn-disallow').on('click', function (e) {
            var form_input = document.getElementById('form-disallow');
            var data = getDataDisallow();
            console.log(data);
            if (data.length == 0) {
                return false;
            }
            form_input.value = data.join('-');
        });
    });

    function getDataAllow() {
        var data = $('#disallowed-routes').yiiGridView('getSelectedRows');
        if (data.length == 0) {
            alert('Silakan centang data terlebih dahulu!')
        }
        return data;
    }

    function getDataDisallow() {
        var data = $('#allowed-routes').yiiGridView('getSelectedRows');
        if (data.length == 0) {
            alert('Silakan centang data terlebih dahulu!')
        }
        return data;
    }
</script>
<?php JSRegister::end(); ?>