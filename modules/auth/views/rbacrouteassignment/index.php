<?php

use richardfan\widget\JSRegister;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Route Assignment';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php JSRegister::begin(); ?>
<script>
    $('.btn-modal').click(function (e) {
        e.preventDefault();
        $('#modal-content').html('Please Wait ...');
        $('#modal-form').modal('show');

        $.ajax({
            url: $(this).attr('url'),
            method: 'post',
            dataType: 'html',
            success: function (data) {
                $('#modal-content').html(data);
            },
            error: function (xhr, error) {
                $('#modal-form').modal('hide');
                alert('Gagal, silakan coba kembali.');
            }

        });

    });
</script>
<?php JSRegister::end(); ?>
<div class="auth-item-child-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p class="pull-right">
        <?= Html::a(' Create Privilege', '#', ['url' => Url::to(['create'], true), 'class' => 'btn-modal btn btn-success pull-right']); ?>
    </p>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name:ntext:Privilege',
            'description',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a(Html::icon('pencil'), '#', ['url' => Url::to(['update', 'id' => $model->name], true), 'class' => 'btn-modal btn btn-sm btn-primary']);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('Assign', ['view', 'id' => $model->name], ['class' => 'btn btn-sm btn-info']);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(Html::icon('trash'), ['delete', 'id' => $model->name], [
                                    'class' => 'btn btn-sm btn-danger',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                        ]]);
                    },
                ],
            ],
        ],
    ]);
    ?>


</div>
<?php
Modal::begin([
    'header' => '<h3> Privilege</h3>',
    'id' => 'modal-form',
    'size' => 'modal-lg',
]);
echo "<div id='modal-content'>Please Wait ...</div>";
Modal::end();
?>