<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\userdb */

$this->title = 'Update Userdb: ' . $model->iduser;
$this->params['breadcrumbs'][] = ['label' => 'Userdbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iduser, 'url' => ['view', 'id' => $model->iduser]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="userdb-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 
        'privilege' => $privilege, 
    ]) ?>

</div>
