<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Route Permission';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-index">
    <p class="pull-right">
        <?= Html::a('Create Route', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    $columns = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'yii\grid\CheckboxColumn',
            'checkboxOptions' => function ($model, $key, $index, $column) {
                if ($model['type'] == 2) {
                    return ['value' => $key];
                }
                return ['style' => ['display' => 'none'], 'disabled' => true];
            },
            'contentOptions' => function ($row) {
                return $row["type"] == 2 ? [] : ($row["type"] == 10 ? ['style' => 'background-color:#59b2ff;color:#fff;'] : []);
            }
        ],
        [
            'label' => 'Route',
            'format' => 'html',
            'value' => function($row) {
                return $row["type"] == 2 ? $row['name'] : ($row["type"] == 10 ? $row['module'] . ' <code>Module</code>' : $row['controller'] . ' <code>Controller</code>');
            },
            'contentOptions' => function ($row) {
                return $row["type"] == 2 ? [] : ($row["type"] == 10 ? ['style' => 'background-color:#59b2ff;color:#fff;'] : ['style' => 'background-color:#868282;color:#fff;']);
            }
        ],
        [
            'label' => 'description',
            'format' => 'html',
            'value' => function($row) {
                return isset($row["description"]) ? $row["description"] : null;
            },
            'contentOptions' => function ($row) {
                return $row["type"] == 2 ? [] : ['style' => 'display:none;'];
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => function ($row) {
                return $row["type"] == 2 ? [] : ['style' => 'display:none;'];
            }
        ],
    ];
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
    ]);
    ?>

</div>
