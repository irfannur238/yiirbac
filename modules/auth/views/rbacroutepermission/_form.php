<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\auth\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auth-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Module & controller Ex. <code>module/controller</code>') ?>

    <?= $form->field($model, 'actions')->textarea(['rows' => 3, 'required' => true])->label('Actions. For multiple Ex. <code>index;create;update</code>') ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?php
    /*
      $form->field($model, 'type')->textInput();

      $form->field($model, 'rule_name')->textInput(['maxlength' => true]);

      $form->field($model, 'data')->textInput();

      $form->field($model, 'created_at')->textInput();

      $form->field($model, 'updated_at')->textInput();
     * 
     */
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
