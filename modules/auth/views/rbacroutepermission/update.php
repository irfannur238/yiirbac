<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\auth\models\AuthItem */

$this->title = 'Update Route Permission: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Route Permission', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->name]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="auth-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formUp', [
        'model' => $model,
    ]) ?>

</div>
