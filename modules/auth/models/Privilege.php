<?php

namespace app\modules\auth\models;

use Yii;

/**
 * This is the model class for table "privilege".
 *
 * @property int $idprivilege
 * @property string $privilege
 * @property string|null $note
 */
class Privilege extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'privilege';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['privilege'], 'required'],
            [['privilege'], 'string', 'max' => 50],
            [['note'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idprivilege' => 'Idprivilege',
            'privilege' => 'Privilege',
            'note' => 'Note',
        ];
    }
}
